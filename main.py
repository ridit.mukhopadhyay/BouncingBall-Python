import pygame
import time
import random

from pygame.locals import (
    K_LEFT,
    K_RIGHT,
    RLEACCEL,
    
    )




pygame.init()

color_white = (250,250,250)
color_black = (0,0,0)

fps = 120
clock = pygame.time.Clock()
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

background = pygame.image.load("bouncingballbackground.png")

game_window = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))
pygame.display.set_caption("bouncing ball")



class Paddle(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("paddle1.png").convert_alpha()
        self.rect = self.image.get_rect(center = (SCREEN_WIDTH/2,SCREEN_HEIGHT - 50))
        self.change_x = 0    
        
    def update(self):
        if self.rect.x < 0:
            self.rect.x = 0
        elif self.rect.x >= 725:
            self.rect.x = 725
        self.change_x = 0
        keystate = pygame.key.get_pressed()
        
        if keystate[K_LEFT]:
            self.change_x = -10
            
        if keystate[K_RIGHT]:
            self.change_x = 10
            
        self.rect.x += self.change_x
        
        
        
class Brick(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("brick.png").convert_alpha()
        self.rect = self.image.get_rect(center =(random.randint(100,700),random.randint(100,SCREEN_HEIGHT/2)))
        
    
class Ball(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("ball.png").convert_alpha()
        self.rect = self.image.get_rect(center=(SCREEN_WIDTH/2,SCREEN_HEIGHT/2))
        self.change_x = random.choice([-3,-2,2,3])
        self.change_y = random.choice([1,2,3])
        
    def update(self):
        self.rect.x += self.change_x
        self.rect.y += self.change_y
        
        if self.rect.top < 0:
            self.change_y *= -1
        if(ball.rect.bottom > SCREEN_HEIGHT ):
            self.change_y = 0
            self.change_x = 0
        if self.rect.left < 0:
            self.change_x *= -1
        if self.rect.right > SCREEN_WIDTH:
            self.change_x *= -1
            
        collision = pygame.sprite.spritecollideany(ball, paddle_sprite) 
        if collision:
            self.change_y *= -1
        
        for brick in brick_sprite_group:   
            collisionbrick = pygame.sprite.spritecollide(ball, brick_sprite_group, True) 
            if collisionbrick:
                self.change_y *= -1
                score.scorevalue +=1
            if score.scorevalue == 10:
                self.change_y = 0
                self.change_x = 0
class Score():
    def __init__(self):
        self.scorevalue = 0
        self.scorefont = pygame.font.SysFont(None,100)
        self.you_win_font = pygame.font.SysFont(None,50)
        
    def update(self):
        self.scoreToBePainted = self.scorefont.render(str(self.scorevalue),True,color_black,)
        self.you_win = self.you_win_font.render("YOU WIN",True,color_black,)
        self.you_lose = self.you_win_font.render("YOU LOSE",True,color_black,)
        
    def draw(self):
        game_window.blit(self.scoreToBePainted,(30,30))
        if self.scorevalue == 10:
            game_window.blit(self.you_win,(SCREEN_WIDTH/2 - 50,SCREEN_HEIGHT/7))
        if (ball.rect.bottom > SCREEN_HEIGHT):
            game_window.blit(self.you_lose,(SCREEN_WIDTH/2 - 50,SCREEN_HEIGHT/7))
    
               
paddle_sprite = pygame.sprite.GroupSingle()
brick_sprite_group = pygame.sprite.Group()
ball_sprite = pygame.sprite.GroupSingle()

paddle = Paddle()
brick1 = Brick()
brick2 = Brick()
brick3 = Brick()
brick4 = Brick()
brick5 = Brick()
brick6 = Brick()
brick7 = Brick()
brick8 = Brick()
brick9 = Brick()
brick10 = Brick()
score = Score()
ball = Ball()
paddle_sprite.add(paddle)
brick_sprite_group.add(brick1)
brick_sprite_group.add(brick2)
brick_sprite_group.add(brick3)
brick_sprite_group.add(brick4)
brick_sprite_group.add(brick5)
brick_sprite_group.add(brick6)
brick_sprite_group.add(brick7)
brick_sprite_group.add(brick8)
brick_sprite_group.add(brick9)
brick_sprite_group.add(brick10)
ball_sprite.add(ball)


running = True
while running:
    clock.tick(fps)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
            
            
            
    paddle_sprite.update()
    ball_sprite.update()
    score.update()
    game_window.fill(color_white)
    game_window.blit(background,(0,0))
    ball_sprite.draw(game_window)
    score.draw()
    brick_sprite_group.draw(game_window)
    paddle_sprite.draw(game_window)
    pygame.display.flip()
    
    
    
    
    
    